ekstrak
=======

A PDF text extraction library.

Purpose
-------

The goal is to have a library that not only extracts text from PDF documents (I recommend Apache Tika if you are only interested in the plain text), but to also get semantic information about the document.

Internally, PDFBox is used to parse the document and create a w3c-DOM model. This is then preparsed into a document dconsisting of pages and lines (I call this the P-model), which is then again analyzed to create the final document that has a hierarchical structure (which I call the S-model).

This library is still in early development and needs some input to correctly identify document headlines. I might or might not have the time to come up with an automatic detection of document styles later.

For the time being, please take a look at the ParserTest class to find out how to use.

Using
-----

ekstrak is available on JCenter and I'll add it to Maven central repository soon.

Building
--------

Clone the repository and run `./gradlew` (macOS, Linux) or `gradlew` (Windows) from within the project folder. Java 8 is required to build.

Changes
-------

### current development

- dependency updates
- command line argument "--help" to show help text

### Version 0.14

 - Java 11
 - update dependencies

### Version 0.12

 - dependency and plugin updates
 - insert word separators when necessary
 - fix: not vlearing rectangles list leads to stray strikethrough and underline text extraction 

### Version 0.11.x

 - dependency and plugin updates

### Version 0.10.x

 - some minor API changes, CSS and HTML-related additions, bugfixes
 - update to latest pdfbox (0.10.6)

### Version 0.9.x

 - smaller changes and bugfixes

### Version 0.9

 - only one constructor, configuration via ParseOptions
 - update dependencies
 - use RichText for retrieving document text instead of plain text/HTML

### Version 0.8.4

 - update dependencies

### Version 0.8.3

 - updates and minor cleanups

### Version 0.8.2.1

 - use Path instead of URI in PdfParser.main()
 
### Version 0.8.2

 - add PdfParser.main() for converting to HTML

### Version 0.8.1

 - fix HTML generation with section header mapping
 
### Version 0.8

 - map section title to section name (i. e. to map action section titles to logical section names)
 - change type of `getSectionLevel` callback from `Function<PLine,Integer>` to `ToIntFunction<PLine>`

### Version 0.7

 - update utility
 - change PdfParser constructor parameters
 
### Version 0.6

 - remove style config per section level
 - fix tag generation for Style class
 - fix CSS colors
 
### Version 0.5.1

 - fix NPE during underline detection for some PDF
 
### Version 0.5

 - update utility
 - rework font handling
 - some performance improvements

### Version 0.4

 - supply options to PdfParser constructor
 - removed now obsolete class ParseException

### Version 0.3.3

 - fix header/footer/content area calculation in constructor

### Version 0.3.2

 - fix NPE when fontdescriptor is missing
 
### Version 0.3.1.1

 - update utility (to be consistent with other libs)

### Version 0.3.1

 - fix an excpetion with font handling
 
### Version 0.3

 - complete rewrite of the pre-parser (no intermediate XML representation needed)

### Version 0.2.10

 - remove use of obsolete collection classes
 - code cleanup
 - update utility

### Version 0.2.9

 - fix multi-line headline detection

### Version 0.2.8

 - close styles at line end 

### Version 0.2.7

 - suppress repeating styles

### Version 0.2.6

 - code cleanup
 - faster evaluation in PdfParser.getValueInPoints()

### Version 0.2.5

 - allow negative values in PdfParser.getValueInPoints()
 
### Version 0.2.4

 - update utility
 - LineStart.getIndent()
 
### Version 0.2.3

 - Document.getSection(String)
 - Section.getSubSection(String)
 - javadoc
 
### Version 0.2.2

 - Document.getRoot() retrieves the root section of the document
 - Section: new methods appendTextContentText(), appendTextContentHtml(), getTextContentText(), getTextContentHtml();
 
### Version 0.2.1

 - add README.md
 - add Javadoc
 - add methods Section.getHtml(), Section.getText()
 
### Version 0.2

 - introduce type hierarchy for the S-Model
 - change package structure
 - add test
 
### Version 0.1

 - initial release
