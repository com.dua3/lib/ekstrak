module dua3_ekstrak {
    exports com.dua3.ekstrak;
    exports com.dua3.ekstrak.model;

    requires java.logging;
    requires dua3_utility;
    requires java.desktop;
    requires org.apache.pdfbox;
}
