/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.*;
import com.dua3.utility.data.Pair;
import com.dua3.utility.io.IOUtil;
import com.dua3.utility.lang.LangUtil;
import com.dua3.utility.options.Arguments;
import com.dua3.utility.options.ArgumentsParser;
import com.dua3.utility.options.Flag;
import com.dua3.utility.options.Option;
import com.dua3.utility.options.OptionException;
import com.dua3.utility.options.SimpleOption;
import com.dua3.utility.text.*;
import com.dua3.utility.text.Font;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.TextPosition;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PdfParser {
    private static final Logger LOG = Logger.getLogger(PdfParser.class.getName());
    private static final Style DEFAULT_FONT_STYLE = Style.create("text", Map.entry(Style.FONT, new Font("Arial-10")));

    public static void main(String[] args) {
        ArgumentsParser cmd = new ArgumentsParser(PdfParser.class.getSimpleName(), "Parse PDF files.");
        try {
            Flag oHelp = cmd.flag("--help", "-h").description("print help and exit");
            SimpleOption<Path> oInput = cmd.simpleOption(Path.class, "--input", "-i").description("set location of input PDF");
            SimpleOption<Path> oOutput = cmd.simpleOption(Path.class, "--output", "-o").description("set location of output PDF");
            Flag oPrint = cmd.flag("--print", "-p").description("print document to stdout");
            SimpleOption<Font> oTextFont = cmd.simpleOption(Font.class, "--text-font", "-tf").description("set text font").defaultValue(getFont(DEFAULT_FONT_STYLE));
            Option<String> oHeaderFont = cmd.option(String.class, "--header-font", "-hf").description("set header font").arity(2);
            Option<String> oFontMap = cmd.option(String.class, "--map-font", "-mf").description("map font family name to other font family name").arity(2);
            Flag oKeepLineBreaks = cmd.flag("--break-lines", "-b").description("insert <br> tags at line ends");

            Arguments cmdArgs = cmd.parse(args);

            if (cmdArgs.isSet(oHelp)) {
                System.out.println(cmd.help());
                System.exit(0);
            }
            
            Path input = cmdArgs.getOrThrow(oInput);
            Optional<Path> output = cmdArgs.get(oOutput);

            // default settings
            Font defaultFont = cmdArgs.getOrThrow(oTextFont);
            Rectangle2D contentArea = new Rectangle2D.Double(0, 0, TextUtil.mm2pt(210), TextUtil.mm2pt(297));

            // define font mappings
            Map<String, String> fontMap = new HashMap<>();
            cmdArgs.stream(oFontMap).forEach(parms -> {
                String from = parms.get(0);
                String to = parms.get(1);
                fontMap.put(from, to);
            });

            // define parse options
            List<ParseOption> parseOptions = new LinkedList<>();

            parseOptions.add(contentRegion(contentArea));
            parseOptions.add(defaultFont(defaultFont));
            parseOptions.add(fontMapper(s -> fontMap.getOrDefault(s, s)));

            cmdArgs.stream(oHeaderFont).forEach(parms -> {
                int level = Integer.parseInt(parms.get(0));
                String fontspec = new Font(parms.get(1)).fontspec();
                parseOptions.add(headerStyle(level, FontDef.parseFontspec(fontspec)));
            });

            // define conversion options
            List<HtmlConversionOption> conversionOptions = new LinkedList<>();
            conversionOptions.add(
                    keepLineBreaks(cmdArgs.isSet(oKeepLineBreaks))
            );

            // do the conversion
            try (InputStream in = Files.newInputStream(input)) {
                PdfParser parser = PdfParser.create(parseOptions);
                Document document = parser.parse(in);
                // write output to file
                output.ifPresent(LangUtil.uncheckedConsumer(out -> IOUtil.write(out, document.getHtml(conversionOptions))));
                // write to stdout
                cmdArgs.ifSet(oPrint, () -> System.out.println(AnsiConverter.create().convert(document.getText())));
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        } catch (OptionException e) {
            System.err.println(cmd.errorMessage(e));
            System.err.println("(run with --help for help)");
            System.exit(1);
        }
    }

    public static HtmlConversionOption keepLineBreaks(boolean flag) {
        return HtmlConverter.map(LineEnd.ATTR_LINE_END, v -> HtmlTag.tag("<br>", ""));
    }

    /**
     * Parser configuration Option: sort texts by position when extracting.
     * @param flag true if texts should be extracted in positional order rather than insertion order 
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption useStandardFontNames(boolean flag) {
        return new ParseOption(parser -> parser.setUseStandardFontNames(flag));
    }

    /**
     * Parser configuration Option: sort texts by position when extracting.
     * @param flag true if texts should be extracted in positional order rather than insertion order 
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption sortByPosition(boolean flag) {
        return new ParseOption(parser -> parser.setFontFamilyMapper(flag));
    }

    /**
     * Parser configuration Option: define content region.
     * @param region the content region; text outside of the content region is discarded  
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption contentRegion(Shape region) {
        return new ParseOption(parser -> parser.setContentRegion(region));
    }

    /**
     * Parser configuration Option: map font family names (i. e. "ArialMT" to "Arial").
     * @param mapper the mapper to use 
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption fontMapper(Function<String, String> mapper) {
        return new ParseOption(parser -> parser.setFontFamilyMapper(mapper));
    }

    /**
     * Parser configuration Option: map input line to section level (i. e. document headline to 'h1': 1).
     * @param mapper the mapper to use 
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption sectionLevelMapper(ToIntFunction<PLine> mapper) {
        return new ParseOption(parser -> parser.setSectionLevelMapper(mapper));
    }

    /**
     * Parser configuration Option: map input line to section name.
     * @param mapper the mapper to use 
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption sectionMapper(Function<String, String> mapper) {
        return new ParseOption(parser -> parser.setSectionMapper(mapper));
    }

    /**
     * Parser configuration Option: define default font for text.
     * @param font the default {@link Font} 
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption defaultFont(Font font) {
        return new ParseOption(parser -> parser.setDefaultFontStyle(Style.create("text", Map.entry(Style.FONT, font))));
    }

    /**
     * Define header style.
     * @param level the header level (1..6)
     * @param font the header font
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption headerStyle(int level, FontDef font) {
        return new ParseOption(parser -> parser.defineHeaderStyle(level, font));    
    }

    /**
     * Define text style.
     * @param cssClass the CSS class name to define
     * @param font the font
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption defineTextStyle(String cssClass, FontDef font) {
        return new ParseOption(parser -> {
            final String regexCssClass = "[a-z0-9_-]*";
            LangUtil.check(cssClass.matches(regexCssClass), "class name must match '%s': %s", regexCssClass, cssClass);

            StyleDefinition sd = new StyleDefinition(
                    font,
                    0,
                    cssClass,
                    String.format(".%s { %s }", cssClass, font.getCssStyle()),
                    String.format("<span class=\"%s\">", cssClass),
                    String.format("</span>")
            );

            parser.addStyleDefinition(sd);
        });
    }
    
    /**
     * Define tag style.
     * @param tagName the tag name
     * @param font the font
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption defineTagStyle(String tagName, FontDef font) {
        return new ParseOption(parser -> parser.defineTagStyle(tagName, "", font));
    }

    /**
     * Define tag style.
     * @param tagName the tag name
     * @param font the font
     * @param addtitionalCss additional CSS to include in the CSS rule
     * @return the option to pass to the PdfParser constructor
     */
    static public ParseOption defineTagStyle(String tagName, FontDef font, String addtitionalCss) {
        return new ParseOption(parser -> parser.defineTagStyle(tagName, addtitionalCss, font));
    }

    /* the following methods are called by ParseOption */
    void setContentRegion(Shape region) {
        this.contentRegion = Objects.requireNonNull(region);
        PdfParser.LOG.fine(() -> "ContentRegion: " + region);
    }

    void setUseStandardFontNames(boolean flag) {
        LOG.fine(() -> "UseStandardFontNames: " + flag);
        stripper.setUseStandardFontNames(flag);
    }

    void setFontFamilyMapper(boolean flag) {
        LOG.fine(() -> "SortByPosition: " + flag);
        stripper.setSortByPosition(flag);
    }

    void setFontFamilyMapper(Function<String, String> mapper) {
        PdfParser.LOG.info(() -> "MapFontFamily: " + mapper);
        stripper.setFontFamilyMapper(mapper);
    }

    void setSectionLevelMapper(ToIntFunction<PLine> mapper) {
        LOG.fine(() -> "SectionLevelMapper: " + mapper);
        this.getSectionLevel = Objects.requireNonNull(mapper);
    }

    void setSectionMapper(Function<String, String> mapper) {
        LOG.fine(() -> "SectionMapper: " + mapper);
        this.sectionMapper = Objects.requireNonNull(mapper);
    }

    void setDefaultFontStyle(Style fontStyle) {
        LOG.fine(() -> "DefaultFont: " + fontStyle);
        this.defaultFontStyle = Objects.requireNonNull(fontStyle);
    }

    /* --- */
    private ToIntFunction<PLine> getSectionLevel = this::defaultSectionMapper;

    private Function<String,String> sectionMapper = s -> s;
    private Style defaultFontStyle = DEFAULT_FONT_STYLE;
    private Shape contentRegion = new Rectangle2D.Double(0, 0, Double.MAX_VALUE, Double.MAX_VALUE);

    private final PDFStyledTextStripper stripper;

    private PDocument pdoc;
    private PPage currentPage = null;

    /**
     * Create instance. 
     * @param options the options to configure the parser, see {@link ParseOption}
     * @return new PdfParser instance
     */
    public static PdfParser create(ParseOption... options) {
        return create(Arrays.asList(options));
    }
    
    /**
     * Create instance. 
     * @param options the options to configure the parser, see {@link ParseOption}
     * @return new PdfParser instance
     */
    public static PdfParser create(Collection<ParseOption> options) {
        PdfParser parser = new PdfParser();

        // apply options
        for (ParseOption option: options) {
            option.apply(parser);
        }
        
        return parser;
    }
    
    /**
     * Constructor.
     */
    private PdfParser() {
        // setup Stripper
        try {
            this.stripper = new PDFStyledTextStripper(this::handleText, this::appendText, this::handlePageStart);
        } catch (IOException e) {
            throw new IllegalStateException("IOException during initialization of "+PDFStyledTextStripper.class.getName(), e);
        }
        
    }

    /**
     * Get the default style used for text.
     * @return the default style
     */
    public Style getDefaultStyle() {
        return defaultFontStyle;
    }

    private void handlePageStart(PDPage pdPage) {
        currentPage = pdoc.startPage();
    }

    private PLine currentLine = null;
    private void handleText(List<TextPosition> textPositions) {
        LangUtil.check(currentPage!=null, "internal error: page is not set");
        textPositions.forEach(tp -> {
            Style style = stripper.getFont(tp);
            float x = tp.getX();
            float w = tp.getWidth();
            float y = tp.getY();
            float h = tp.getHeight();
            String text = tp.getUnicode();
            currentLine = currentPage.getLine(y, x);
            currentLine.addText(style, x, w, h, text.equals("\n") ? " " : text);
        });
    }

    private void appendText(String text) {
        LangUtil.check(currentPage!=null, "internal error: page is not set");
        LangUtil.check(currentLine!=null, "internal error: line is not set");
        currentLine.currentText().append(text);
    }

    /**
     * Parse PDF from InputStream.
     * @param in    the InputStream with the PDF data
     * @return the parsed document
     * @throws IOException on i/o error
     */
    public Document parse(InputStream in) throws IOException {
        LOG.fine("loading PDF ...");
        try (PDDocument pdf = PDDocument.load(in)) {
            LOG.fine("preparsing ...");
            preparse(pdf);

            LOG.fine("converting to document ...");
            return convert(pdoc);
        }
    }

    /**
     * Convert a P-model document into the semantic model.
     *
     * @param pdoc the p-model document
     * @return the semantic model document
     */
    private Document convert(PDocument pdoc) {
        // create the document
        Document doc = new Document();
        
        // set the header styles
        predefinedStyles.forEach(sd -> {
            if (sd.headerLevel>0) {
                doc.setHeaderStyle(sd.headerLevel, sd.font);
            }
        });
        
        // collect lines
        List<Pair<Integer, PLine>> lines = new ArrayList<>();
        PLine rootLine = new PLine(null, 0, 0, 0);
        rootLine.addText(defaultFontStyle, 0, 0, 0, "");
        lines.add(Pair.of(0, rootLine)); // extra line for root node

        pdoc.nodes()
                .filter(n -> n instanceof PLine)
                .map(n -> (PLine) n)
                // filter region
                .filter(line -> contentRegion.contains(line.getLeft(), line.getTop()))
                // add section level information
                .map(line -> Pair.of(getSectionLevel.applyAsInt(line), line))
                // collect headers
                .forEach(lines::add);

        // create stack of sections 
        LOG.finer(() -> "creating stack and adding root section");
        Deque<Section> stack = new LinkedList<>();

        int i = 0;
        while (i < lines.size()) {
            Pair<Integer, PLine> line = lines.get(i);

            // start section
            int level = line.first;

            // pop lower level sections from stack
            while (!stack.isEmpty() && level <= stack.peekLast().getLevel()) {
                LOG.finer("pop section with level " + stack.peekLast().getLevel());
                stack.pollLast();
            }

            // determine header lines (title might be split across several lines)
            int idxHeaderStart = i;
            int idxHeaderEnd = idxHeaderStart+1;
            double lineBottom = line.second.getBottom();
            double lineHeight = line.second.getHeight();
            while (idxHeaderEnd < lines.size()
                   // same level
                   && level != 0
                   && Objects.equals(lines.get(idxHeaderEnd).first, level)
                   && (lines.get(idxHeaderEnd).second.getTop()-lineBottom) < lineHeight*0.99) {
                lineBottom = lines.get(idxHeaderEnd).second.getBottom();
                ++idxHeaderEnd;
            }

            int idxContentStart = idxHeaderEnd;
            int idxContentEnd = idxContentStart;
            while (idxContentEnd < lines.size() && lines.get(idxContentEnd).first == 0) {
                ++idxContentEnd;
            }

            // extract section title
            String title = lines.subList(idxHeaderStart, idxHeaderEnd)
                    .stream()
                    .map(p -> p.second)
                    .map(PNode::getText)
                    .collect(Collectors.joining(" "));

            // determine section name
            String name = sectionMapper.apply(title);
            
            // create section object and add to stack
            Section parent = stack.isEmpty() ? null : stack.peekLast();
            Section s = new Section(doc, parent, line.first, name, title, defaultFontStyle);

            // add header lines
            addSectionLines(s, line.second.style().getFontStyle(), lines.subList(idxHeaderStart, idxHeaderEnd));
            s.markHeaderEnd();

            // add content lines
            addSectionLines(s, defaultFontStyle, lines.subList(idxContentStart, idxContentEnd));

            // add section
            if (parent == null) {
                LangUtil.check(stack.isEmpty(), "only one root element allowed");
            } else {
                LOG.finer("adding section '" + s.getName() + "' to section '" + parent.getName() + "'");
                parent.addSection(s);
            }
            LOG.finer("pushing section '" + s.getName() + "' onto stack");
            stack.add(s);

            // move on
            i = idxContentEnd;
        }

        doc.setRoot(stack.getFirst());
        
        return doc;
    }

    private void addSectionLines(Section s, Style fontStyle, Collection<Pair<Integer, PLine>> lines) {
        Deque<Node> nodeStack = new LinkedList<>();
        nodeStack.add(s);
        lines.stream()
                .map(line -> line.second)
                .forEach(line -> {
                    Node parent = nodeStack.peekLast();
                    nodeStack.peekLast().add(new LineStart(parent, line.getIndent()));

                    Deque<Style> currentFontStyle = new LinkedList<>();
                    currentFontStyle.push(fontStyle);

                    for (PNode node : line.children()) {
                        // handle styles
                        if (node instanceof PStyle) {
                            PStyle style = (PStyle) node;
                            Style newFontStyle = style.getFontStyle();
                            if (Font.similar(getFont(newFontStyle), getFont(currentFontStyle.peek()))) {
                                // filter out obsolete fontStyle definitions
                                continue;
                            } else {
                                // close current styles
                                closeCurrentStyles(currentFontStyle, nodeStack, newFontStyle);

                                // if we are back to the section default fontStyle, there's nothing more to do
                                if (newFontStyle.equals(currentFontStyle.peekLast())) {
                                    // filter out obsolete fontStyle definitions
                                    continue;
                                }

                                // otherwise create a new style
                                Styled sStyled = new Styled(nodeStack.peekLast(), newFontStyle);
                                nodeStack.peekLast().add(sStyled);
                                nodeStack.add(sStyled);
                                currentFontStyle.push(newFontStyle);
                            }
                        } else if (node instanceof PText) {
                            PText pText = (PText) node;
                            Text sText = new Text(nodeStack.peekLast(), pText.getText());
                            nodeStack.peekLast().add(sText);
                        } else {
                            throw new IllegalStateException("unsupported PNode type: " + node.getClass());
                        }
                    }

                    while (nodeStack.peekLast() != parent) {
                        nodeStack.pollLast();
                    }
                    nodeStack.peekLast().add(new LineEnd(nodeStack.peekLast()));
                });
    }

    private void closeCurrentStyles(Deque<Style> currentFont, Deque<? extends Node> nodeStack, Style newFont) {
        while (currentFont.size() > 1 && !currentFont.peek().equals(newFont)) {
            while (nodeStack.size()>1 &&
                   (!(nodeStack.peekLast() instanceof Styled)
                    || !nodeStack.peekLast().getFontStyle().equals(currentFont.peekLast()))) {
                nodeStack.pollLast();
            }
            currentFont.pollLast();
        }
    }

    private void preparse(PDDocument doc) throws IOException {
        pdoc = new PDocument();
        stripper.writeText(doc, nullWriter());
    }

    private static Writer nullWriter() {
        return new Writer() {
            @Override
            public Writer append(char c) {
                // nop
                return this;
            }

            @Override
            public Writer append(CharSequence csq) {
                // nop
                return this;
            }

            @Override
            public Writer append(CharSequence csq, int start, int end) {
                // nop
                return this;
            }

            @Override
            public void write(int c) {
                // nop
            }

            @Override
            public void write(char[] cbuf, int off, int len) {
                // nop
            }

            @Override
            public void write(String str) {
                // nop
            }

            @Override
            public void write(String str, int off, int len) {
                // nop
            }

            @Override
            public void flush() {
                // nop
            }

            @Override
            public void close() {
                // nop
            }
        };
    }

    // --- style handling ---
    static class StyleDefinition {
        final FontDef font;
        final int headerLevel;
        final String cssClass;
        final String cssDefinition;
        final String htmlOpen;
        final String htmlClose;

        public StyleDefinition(FontDef font, int headerLevel, String cssClass, String cssDefinition, String htmlOpen, String htmlClose) {
            this.font = Objects.requireNonNull(font);
            this.headerLevel = headerLevel;
            this.cssClass = cssClass;
            this.cssDefinition = cssDefinition;
            this.htmlOpen = htmlOpen;
            this.htmlClose = htmlClose;
        }
    }

    private final List<StyleDefinition> predefinedStyles = new ArrayList<>();

    private void addStyleDefinition(StyleDefinition sd) {
        predefinedStyles.add(sd);
    }

    void defineHeaderStyle(int headerLevel, FontDef font) {
        LangUtil.check(headerLevel>=1 && headerLevel <=6, "level must be between 1 and 6: %d", headerLevel);
        StyleDefinition sd = new StyleDefinition(
                font,
                headerLevel,
                null,
                String.format("h%d { %s }", headerLevel, font.getCssStyle()),
                String.format("<h%d>", headerLevel),
                String.format("</h%d>", headerLevel)
        );

        addStyleDefinition(sd);
    }

    private static final String REGEX_TAG_NAME = "[a-z]+[a-z0-9]*";
    void defineTagStyle(String tagName, String additionalCss, FontDef font) {
        LangUtil.check(tagName.matches(REGEX_TAG_NAME), "tag name must match '%s': %s", REGEX_TAG_NAME, tagName);

        StyleDefinition sd = new StyleDefinition(
                font,
                0,
                null,
                String.format("%s { %s }", tagName, font.getCssStyle()+additionalCss),
                String.format("<%s>", tagName),
                String.format("</%s>", tagName)
        );

        addStyleDefinition(sd);
    }

    private int defaultSectionMapper(PLine pLine) {
        Style fontStyle = pLine.style().getFontStyle();
        for (StyleDefinition sd: predefinedStyles) {
            if (sd.headerLevel!=0 && sd.font.matches(getFont(fontStyle))) {
                return sd.headerLevel;
            }
        }
        // no matching header style found
        return 0;
    }
    
    public static Font getFont(Style fontStyle) {
        return (Font) fontStyle.get(Style.FONT);
    }

}
