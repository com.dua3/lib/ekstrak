/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.utility.data.Pair;
import com.dua3.utility.text.Font;
import com.dua3.utility.text.FontDef;
import com.dua3.utility.text.Style;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.operator.OperatorProcessor;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;

/**
 * A class that implements a style-aware PDF text stripper, based on 
 * <a href="https://github.com/mkl-public/testarea-pdfbox1/tree/master/src/main/java/mkl/testarea/pdfbox1/extract">
 * code published by mkl under ASL 2 on Github</a>. 
 */
public class PDFStyledTextStripper extends PDFTextStripper {
    private static final Logger LOG = Logger.getLogger(PDFStyledTextStripper.class.getName());
    
    private Function<String, String> fontFamilyMapper = s -> s;
    
    final List<TransformedRectangle> rectangles = new ArrayList<>();
    private final Consumer<PDPage> pageStartHandler;
    private final Consumer<List<TextPosition>> textHandler;
    private final Consumer<String> textAppender;
    private boolean useStandardFontNames = true;

    public PDFStyledTextStripper(Consumer<List<TextPosition>> textHandler, Consumer<String> textAppender, Consumer<PDPage> pageStartHandler) throws IOException {
        addOperator(new AppendRectangleToPath());
        this.pageStartHandler = Objects.requireNonNull(pageStartHandler); 
        this.textHandler = Objects.requireNonNull(textHandler);
        this.textAppender = Objects.requireNonNull(textAppender);
    }

    public void setUseStandardFontNames(boolean flag) {
        this.useStandardFontNames = flag;
    }


    public void setFontFamilyMapper(Function<String,String> fontFamilyMapper) {
        this.fontFamilyMapper = Objects.requireNonNull(fontFamilyMapper);
    }
    
    @Override
    protected void startPage(PDPage page) throws IOException {
        super.startPage(page);
        rectangles.clear();
        pageStartHandler.accept(page);
    }

    @Override
    protected void writeString(String text, List<TextPosition> textPositions) {
        textHandler.accept(textPositions);
    }

    @Override
    protected void writeWordSeparator() throws IOException {
        textAppender.accept(getWordSeparator());
    }

    private final Font baseFont = new Font();

    private static final class FontKey {
        final PDFont pdFont;
        final float size;
        final boolean underline;
        final boolean strikethrough;
        
        private final int hash;
        
        FontKey (PDFont pdFont, float size, boolean underline, boolean strikethrough) {
                this.pdFont = pdFont;
                this.size=size;
                this.underline=underline;
                this.strikethrough=strikethrough;
                this.hash=pdFont.hashCode()+Float.hashCode(size)+Boolean.hashCode(strikethrough)+7*Boolean.hashCode(underline);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            FontKey fontKey = (FontKey) o;
            return hash == fontKey.hash && 
                   underline == fontKey.underline && 
                   strikethrough == fontKey.strikethrough && 
                   size ==  fontKey.size &&
                   pdFont.equals(fontKey.pdFont);
        }

        @Override
        public int hashCode() {
            return hash;
        }
    }

    private final Map<FontKey, Style> docStyles = new HashMap<>();
    
    public Style getFont(TextPosition textPosition) {
        PDFont font = textPosition.getFont();
        float sz = textPosition.getFontSizeInPt();
        boolean underline = rectangles.stream().anyMatch(r -> r.underlines(textPosition));
        boolean strikeThrough = rectangles.stream().anyMatch(r -> r.strikesThrough(textPosition));

        FontKey key = new FontKey(font, sz, underline, strikeThrough);

        return docStyles.computeIfAbsent(key, k -> {
            // some fonts are only distinguishable by the name, so make it accessible via toString()
            FontDef fd = new FontDef();

            String fontName = font.getName();
            String fontNameLowerCase = fontName.toLowerCase(Locale.ROOT);

            int famStart = fontName.lastIndexOf('+')+1;
            int famEnd = fontName.indexOf('-', famStart);
            String family = normalizeFontFamily(fontName.substring(famStart, famEnd==-1 ? fontName.length() : famEnd));

            fd.setFamily(family);
            fd.setBold(fontNameLowerCase.contains("bold"));
            fd.setItalic(fontNameLowerCase.contains("italic"));
            fd.setUnderline(k.underline);
            fd.setStrikeThrough(k.strikethrough);
            fd.setSize(k.size);
            Font styleFont = useStandardFontNames 
                    ? baseFont.deriveFont(fd) 
                    : new Font(baseFont, fd) { @Override public String toString() { return fontName+"@"+k.size; } };
            return Style.create(styleFont.toString(), Map.entry(Style.FONT, styleFont));
        });
    }

    private String normalizeFontFamily(String family) {
        return fontFamilyMapper.apply(family);
    }

    static class TransformedRectangle {
        final Point2D p0, p1, p2, p3;

        TransformedRectangle(Point2D p0, Point2D p1, Point2D p2, Point2D p3) {
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        boolean strikesThrough(TextPosition textPosition) {
            Matrix matrix = textPosition.getTextMatrix();
            // TODO: This is a very simplistic implementation only working for horizontal text without page rotation
            // and horizontal rectangular strikeThroughs with p0 at the left bottom and p2 at the right top

            // Check if rectangle horizontally matches (at least) the text
            if (p0.getX() > matrix.getTranslateX() + textPosition.getWidth() * .1f || p2.getX() < matrix.getTranslateX() + textPosition.getWidth() * .9f)
                return false;
            // Check whether rectangle vertically is at the right height to underline
            double vertDiff = p0.getY() - matrix.getTranslateY();
            if (vertDiff < 0) {
                return false;
            }
            PDFontDescriptor fd = textPosition.getFont().getFontDescriptor();
            if (fd == null || vertDiff > fd.getAscent() * textPosition.getFontSizeInPt() / 1000.0) {
                return false;
            }
            // Check whether rectangle is small enough to be a line
            return Math.abs(p2.getY() - p0.getY()) < 2;
        }

        boolean underlines(TextPosition textPosition) {
            Matrix matrix = textPosition.getTextMatrix();
            // TODO: This is a very simplistic implementation only working for horizontal text without page rotation
            // and horizontal rectangular underlines with p0 at the left bottom and p2 at the right top

            // Check if rectangle horizontally matches (at least) the text
            if (p0.getX() > matrix.getTranslateX() + textPosition.getWidth() * .1f || p2.getX() < matrix.getTranslateX() + textPosition.getWidth() * .9f)
                return false;
            // Check whether rectangle vertically is at the right height to underline
            double vertDiff = p0.getY() - matrix.getTranslateY();
            if (vertDiff > 0) {
                return false;
            }
            PDFontDescriptor fd = textPosition.getFont().getFontDescriptor();
            if (fd==null || vertDiff < fd.getDescent() * textPosition.getFontSizeInPt() / 500.0) {
                return false;
            }
            // Check whether rectangle is small enough to be a line
            return Math.abs(p2.getY() - p0.getY()) < 2;
        }
    }

    class AppendRectangleToPath extends OperatorProcessor {
        @Override
        public void process(Operator operator, List<COSBase> operands) {
            COSNumber x = (COSNumber) operands.get(0);
            COSNumber y = (COSNumber) operands.get(1);
            COSNumber w = (COSNumber) operands.get(2);
            COSNumber h = (COSNumber) operands.get(3);

            float x1 = x.floatValue();
            float y1 = y.floatValue();

            // create a pair of coordinates for the transformation
            float x2 = w.floatValue() + x1;
            float y2 = h.floatValue() + y1;

            Point2D p0 = transformedPoint(x1, y1);
            Point2D p1 = transformedPoint(x2, y1);
            Point2D p2 = transformedPoint(x2, y2);
            Point2D p3 = transformedPoint(x1, y2);

            rectangles.add(new TransformedRectangle(p0, p1, p2, p3));
        }

        @Override
        public String getName() {
            return "re";
        }

        Point2D.Float transformedPoint(float x, float y) {
            float[] position = {x, y};
            getGraphicsState().getCurrentTransformationMatrix().createAffineTransform().transform(
                    position, 0, position, 0, 1);
            return new Point2D.Float(position[0], position[1]);
        }
    }
}
