/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.Node;
import com.dua3.utility.text.RichTextBuilder;

/**
 * Line start marker node in the S-Model.
 * <p>
 * NOTE: instances of this class cannot have children.
 */
public class LineStart extends Node {

    private final double indent;

    /**
     * Constructor.
     * @param parent the parent node
     * @param indent the indentation value for the new line in points
     */
    public LineStart(Node parent, double indent) {
        super(parent);
        this.indent = indent;
    }

    @Override
    public void appendTo(RichTextBuilder rtb) {
        for (Node c: children()) {
            c.appendTo(rtb);
        }
    }

    @Override
    public void add(Node child) {
        throw new UnsupportedOperationException(getClass().getSimpleName()+" is a marker class and cannot contain child nodes");
    }

    @Override
    public double getIndent() {
        return indent;
    }
}
