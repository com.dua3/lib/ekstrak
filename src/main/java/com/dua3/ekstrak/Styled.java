/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.Node;
import com.dua3.utility.text.Font;
import com.dua3.utility.text.FontDef;
import com.dua3.utility.text.RichTextBuilder;
import com.dua3.utility.text.Style;

/**
 * Node containing style (font) information in the S-Model.
 */
public class Styled extends Node {
    
    private final Style fontStyle;

    /**
     * Constructor.
     * @param parent the parent node
     * @param fontStyle the font style to be applied to this node's children
     */
    public Styled(Node parent, Style fontStyle) {
        super(parent);
        this.fontStyle = fontStyle;
    }

    private void pushIf(RichTextBuilder rtb, Boolean value, Style ifTrue, Style ifFalse) {
        if (value!=null) {
            rtb.push(value ? ifTrue : ifFalse);
        }
    }
    
    private void popIf(RichTextBuilder rtb, Boolean value, Style ifTrue, Style ifFalse) {
        if (value!=null) {
            rtb.pop(value ? ifTrue : ifFalse);
        }
    }
    
    @Override
    public void appendTo(RichTextBuilder rtb) {
        Style fontStyle = getFontStyle();
        
        Font font = (Font) fontStyle.get(Style.FONT);
        Font parentFont = (Font) parent().getFontStyle().get(Style.FONT);
        FontDef delta = Font.delta(parentFont, font);

        if (delta.getFamily()!=null || delta.getSize()!=null || delta.getColor()!=null) {
            rtb.push(fontStyle);
            children().forEach(c->c.appendTo(rtb));
            rtb.pop(fontStyle);
        } else {
            pushIf(rtb, delta.getBold(), Style.BOLD, Style.NORMAL);
            pushIf(rtb, delta.getItalic(), Style.ITALIC, Style.REGULAR);
            pushIf(rtb, delta.getUnderline(), Style.UNDERLINE, Style.NO_UNDERLINE);
            pushIf(rtb, delta.getStrikeThrough(), Style.LINE_THROUGH, Style.NO_LINE_THROUGH);
            children().forEach(c->c.appendTo(rtb));
            popIf(rtb, delta.getStrikeThrough(), Style.LINE_THROUGH, Style.NO_LINE_THROUGH);
            popIf(rtb, delta.getUnderline(), Style.UNDERLINE, Style.NO_UNDERLINE);
            popIf(rtb, delta.getItalic(), Style.ITALIC, Style.REGULAR);
            popIf(rtb, delta.getBold(), Style.BOLD, Style.NORMAL);
        }
    }

    @Override
    public Style getFontStyle() {
        return this.fontStyle;
    }

    @Override
    public String toString() {
        return "Style{" +
               "font=" + this.fontStyle +
               '}';
    }
}
