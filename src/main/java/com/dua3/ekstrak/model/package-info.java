/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/**
 * This package contains the document model used by the ekstrak library.
 * <p>
 * There are two tears:
 * <ul>
 *     <li> P-classes represent an intermediary, pre-parsed linear model of the document used by the parser, defined
 *     in this package.
 *     <li> The remaining classes represent the final, semantically structured model, define in the parent package.
 * </ul>
 * User code will normally only operate on P-classes to provide input for the preparsing process, i. e. in callbacks
 * that are used to detect page header and footer, and extract headline information.
 */
package com.dua3.ekstrak.model;
