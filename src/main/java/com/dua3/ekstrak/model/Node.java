/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak.model;

import com.dua3.utility.text.RichTextBuilder;
import com.dua3.utility.text.Style;

import java.util.*;

/**
 * Abstract base class for nodes of the S-model.
 */
public abstract class Node extends AbstractNode<Node> {

    private final List<Node> children = new LinkedList<>();

    /**
     * Constructor.
     * @param parent the parent node
     */
    protected Node(Node parent) {
        super(parent);
    }

    /**
     * Check semantic consistency of this node and it's subtree.
     * @return true, if not consistency errors detected
     */
    public boolean isConsistent() {
        return children().stream().allMatch(Node::isConsistent);
    }

    /**
     * Append this nodes text to an {@link com.dua3.utility.text.RichTextBuilder}.
     * @param rtb the {@link RichTextBuilder} to append to
     */
    public abstract void appendTo(RichTextBuilder rtb);

    /**
     * Add child to this node.
     * @param child the node to add
     */
    public void add(Node child) {
        children.add(Objects.requireNonNull(child));
    }

    /**
     * Get this node's child nodes.
     * @return the list of child nodes
     */
    public List<Node> children() {
        return Collections.unmodifiableList(children);
    }

    /**
     * Get this node's indentation (from the left).
     * @return the indentation of this node
     */
    public double getIndent() {
        return parent() == null ? 0.0 : parent().getIndent();
    }

    /**
     * Get font for this node.
     * @return the font for this node
     */
    public Style getFontStyle() {
        Node p = parent();
        return p != null ? p.getFontStyle() : null;
    }
}
