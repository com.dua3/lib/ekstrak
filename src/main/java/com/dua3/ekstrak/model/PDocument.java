/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak.model;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Preparsed document class.
 */
public class PDocument extends PNode {
    private final List<PPage> pages = new LinkedList<>();

    /**
     * Constructor
     */
    public PDocument() {
        super(null);
    }

    /**
     * Add new page to the document.
     * @return new PPage instance
     */
    public PPage startPage() {
        PPage page = new PPage(this, 1 + pages.size());
        pages.add(page);
        return page;
    }
    
    @Override
    public Collection<PNode> children() {
        return Collections.unmodifiableList(pages);
    }

    @Override
    public String toString() {
        return pages.stream().map(Object::toString).collect(Collectors.joining());
    }

    @Override
    public String getText() {
        return pages.stream().map(PNode::toString).collect(Collectors.joining());
    }

}
