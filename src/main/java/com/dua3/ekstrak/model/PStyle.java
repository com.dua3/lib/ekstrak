/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak.model;

import com.dua3.utility.text.Style;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

public class PStyle extends PNode {
    private final Style fontStyle;
    
    public PStyle(PLine parent, Style font) {
        super(parent);
        this.fontStyle = Objects.requireNonNull(font);
    }

    @Override
    public Collection<PNode> children() {
        return Collections.emptyList();
    }

    public Style getFontStyle() {
        return fontStyle;
    }

    @Override
    public String toString() {
        return fontStyle.toString();
    }

    @Override
    public String getText() {
        return "";
    }
}
