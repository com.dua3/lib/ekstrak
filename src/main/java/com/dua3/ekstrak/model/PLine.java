/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak.model;

import com.dua3.utility.lang.LangUtil;
import com.dua3.utility.text.Font;
import com.dua3.utility.text.Style;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PLine extends PNode {
    private static final Logger LOG = Logger.getLogger(PLine.class.getName());
    
    private final int number;
    private final double top;
    private final double indent;
    private double right;
    private double height;

    private final Deque<PNode> children = new LinkedList<>();

    private Style currentFontStyle = null;
    private double spaceWidth = 0.01;
    
    private void setCurrentFontStyle(Style fontStyle) {
        if (!Objects.equals(fontStyle, this.currentFontStyle)) {
            this.currentFontStyle = fontStyle;
            this.spaceWidth = Math.max(0.01, ((Font)fontStyle.get(Style.FONT)).getSpaceWidth());
        }
    }

    public PLine(PPage parent, int number, double top, double indent) {
        super(parent);
        
        this.number = number;
        this.top = top;
        this.indent = indent;
        this.right = indent;
        this.height = 0;
    }

    private PText newText() {
        PText text = new PText(this);
        children.add(text);
        return text;
    }
    
    public PText currentText() {
        if ( children.isEmpty()) {
            return newText();
        }
        
        PNode tail = children.peekLast();
        return tail instanceof PText ? (PText) tail : newText();
    }
    
    public void addText(Style fontStyle, double left, double width, double height, CharSequence t) {
        if (!fontStyle.equals(currentFontStyle)) {
            children.add(new PStyle(this, fontStyle));
            setCurrentFontStyle(fontStyle);
        }
        
        double space = left - right;
        if(space<-width) {
            LOG.warning("inconsistent position - text appended to the left");
        } else if (space>0.01){
            int n = (int) Math.round(space / spaceWidth);
            for (int i=0; i<n; i++) {
                currentText().append(" ");
            }
        }
        
        currentText().append(t);
        this.right = left + width;
        this.height = Math.max(this.height, height);
    }
    
    @Override
    public Collection<PNode> children() {
        return Collections.unmodifiableCollection(children);
    }

    @Override
    public String toString() {
        return children().stream().map(Object::toString).collect(Collectors.joining(" ")).trim();
    }

    @Override
    public String getText() {
        return children().stream().map(PNode::getText).collect(Collectors.joining(" ")).trim();
    }

    public int getNumber() {
        return number;
    }

    public double getTop() {
        return top;
    }

    public double getBottom() {
        return top+height;
    }

    public double getLeft() {
        return indent;
    }

    public double getIndent() {
        return indent;
    }
    
    public double getRight() {
        return right;
    }
    
    public double getWidth() {
        return right-indent;
    }
    
    public double getHeight() {
        return height;
    }

    public PStyle style() {         
        PNode first = children.peekFirst();
        LangUtil.check(first instanceof PStyle, "internal error: first child expected as PStyle but was: " + (first == null?null:first.getClass().getName()));
        return (PStyle) first;
    }
}
