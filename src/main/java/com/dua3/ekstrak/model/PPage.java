/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak.model;

import java.util.*;

public class PPage extends PNode {
    private final int number;
    private final Deque<PLine> children = new LinkedList<>();
    
    public PPage(PDocument parent, int number) {
        super(parent);
        this.number = number;
    }
    
    public Collection<PNode> children() {
        return Collections.unmodifiableCollection(children);
    }

    @Override
    public String toString() {
        try (Formatter fmt = new Formatter()) {
            fmt.format("---------- Page %3d ----------%n", number);
            children.forEach(line -> fmt.format("%s%n", line.toString()));
            return fmt.toString();
        }
    }

    @Override
    public String getText() {
        StringBuilder sb = new StringBuilder();
        children.forEach(line -> sb.append(line.getText()).append("\n"));
        return sb.toString();
    }

    public PLine getLine(double top, double left) {
        if (!children.isEmpty() && children.peekLast().getTop() == top) {
            return children.peekLast();
        }
        
        PLine pline = new PLine(this, children.size() + 1, top, left);
        children.add(pline);
        return pline;
    }

}
