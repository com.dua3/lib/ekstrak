/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak.model;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Abstract base class for nodes of both document models (P-model and S-model).
 */
public abstract class AbstractNode<N extends AbstractNode<N>> {

    private final N parent;

    /**
     * Constructor
     * @param parent the parent node
     */
    protected   AbstractNode(N parent) {
        this.parent = parent;
    }

    /**
     * Get parent of node.
     * @return the parent node or {@code null} if this is the root node
     */
    public N parent() {
        return parent;
    }

    /**
     * Get children of this node.
     * @return list with this node's children  
     */
    public abstract Collection<N> children();

    /**
     * Get stream of all the nodes of the (sub)tree that has this node as it's root.
     * @return Stream of nodes contained in (sub)tree
     */
    @SuppressWarnings("unchecked")
    public Stream<N> nodes() {
        return (Stream<N>) Stream.concat(
                Stream.of(this),
                descendants()
        );
    }

    /**
     * Get stream of all descendant nodes.
     * @return Stream of descendants
     */
    public Stream<N> descendants() {
        return children().stream().flatMap(N::nodes);
    }
}
