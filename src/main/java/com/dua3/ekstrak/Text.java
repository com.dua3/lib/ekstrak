/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.Node;
import com.dua3.utility.text.RichTextBuilder;

/**
 * A text node in the S-Model.
 */
public class Text extends Node {
    private final String text;

    /**
     * Constructor.
     * @param parent the parent node
     * @param text the text
     */
    public Text(Node parent, String text) {
        super(parent);
        this.text = text;
    }

    @Override
    public void appendTo(RichTextBuilder rtb) {
        rtb.append(text);
    }

    @Override
    public String toString() {
        return "Text{" +
               "text='" + text + '\'' +
               '}';
    }
}
