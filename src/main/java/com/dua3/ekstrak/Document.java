/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.utility.data.Color;
import com.dua3.utility.data.Pair;
import com.dua3.utility.lang.LangUtil;
import com.dua3.utility.text.*;

import java.util.*;
import java.util.logging.Logger;

/**
 * Document class in the S-Model.
 */
public class Document {
    private static final Logger LOG = Logger.getLogger(Document.class.getName());
    
    public static final String STYLE_ATTRIBUTE_HEADER_LEVEL = "header-level";
    public static final String CSS_CLASS = "pdf";
    
    private Section root = null;

    private Map<Integer, Style> headerStyles = new HashMap<>();

    /**
     * Get style for headlines.
     * @param level the header level
     * @return the style
     */
    public Style getHeaderStyle(int level) {
        return headerStyles.computeIfAbsent(level, lvl -> Style.create("header-"+lvl, Map.entry(STYLE_ATTRIBUTE_HEADER_LEVEL, lvl)));
    }

    /**
     * Get this document's default text style.
     * @return this document's default text style
     */
    public Style getDefaultStyle() {
        return getRoot().getFontStyle();
    }

    /**
     * Define style for headlines.
     * @param level the header level
     * @return the style
     */
    public Style setHeaderStyle(int level, FontDef fd) {
        String styleName = "header-"+level;
        
        Map<String,Object> attributes = new HashMap<>();
        attributes.put(STYLE_ATTRIBUTE_HEADER_LEVEL, level);
        fd.ifColorDefined(v -> attributes.put(Style.COLOR, v));
        fd.ifSizeDefined(v -> attributes.put(Style.FONT_SIZE, v));
        fd.ifFamilyDefined(v -> attributes.put(Style.FONT_TYPE, v));
        fd.ifBoldDefined(v -> attributes.put(Style.FONT_WEIGHT, Boolean.TRUE.equals(v) ? Style.FONT_WEIGHT_VALUE_BOLD : Style.FONT_WEIGHT_VALUE_NORMAL));
        fd.ifItalicDefined(v -> attributes.put(Style.FONT_STYLE, Boolean.TRUE.equals(v) ? Style.FONT_STYLE_VALUE_ITALIC : Style.FONT_STYLE_VALUE_NORMAL));
        fd.ifUnderlineDefined(v -> attributes.put(Style.TEXT_DECORATION_UNDERLINE, Boolean.TRUE.equals(v) ? Style.TEXT_DECORATION_UNDERLINE_VALUE_LINE : Style.TEXT_DECORATION_UNDERLINE_VALUE_NO_LINE));
        fd.ifStrikeThroughDefined(v -> attributes.put(Style.TEXT_DECORATION_LINE_THROUGH, Boolean.TRUE.equals(v) ? Style.TEXT_DECORATION_LINE_THROUGH_VALUE_LINE : Style.TEXT_DECORATION_LINE_THROUGH_VALUE_NO_LINE));

        Style style = Style.create(styleName, attributes);

        return headerStyles.put(level, style);
    }

    /**
     * Get CSS definitions.
     */
    public String getCss() {
        Formatter fmt = new Formatter();
        headerStyles.forEach( (level, style) -> {
            fmt.format("h%d.%s { %s }%s", level, "pdf", style.getFontDef().getCssStyle(), "\n");
        });
        return fmt.toString();
    }
    
    /**
     * Create new document.
     */
    Document() {
        LOG.fine(() -> "creating new document");
    }

    /**
     * Set the root node
     * @param root the root node
     */
    void setRoot(Section root) {
        LOG.fine(() -> "setting root node '"+root.getName()+"' consisting of "+root.getSubSections().size()+" sections");
        LangUtil.check(this.root==null, "root node already set");
        this.root = Objects.requireNonNull(root);
    }

    /**
     * Get the document root.
     * @return this document's root section
     */
    public Section getRoot() {
        return root;
    }

    /**
     * Get the sections of this document.
     * @return collection containing the document sections
     */
    public Collection<Section> sections() {
        return root.getSubSections();
    }

    /**
     * Get this document's text as {@link com.dua3.utility.text.RichText}.
     * @return the document text
     */
    public RichText getText() {
        RichTextBuilder rtb = new RichTextBuilder();
        root.appendTo(rtb);
        return rtb.toRichText();
    }

    /**
     * Get HTML version of document.
     * @param options the conversiono options to use.
     * @return the HTML for this document
     */
    public String getHtml(List<HtmlConversionOption>  options) {
        // replace the passed in options by a mutable instance so that we can add new mappings
        options = new ArrayList<>(options);
        options.addAll(getHtmlConversionOptions());

        // create converter
        HtmlConverter converter = HtmlConverter.create(options);
        
        // convert
        RichText text = getText();
        return converter.convert(text);
    }

    /**
     * Get {@link HtmlConversionOption}s for accurately converting documents produced by this parser instance.
     * @return the options to use in conversion
     */
    public List<HtmlConversionOption> getHtmlConversionOptions() {
        return Arrays.asList(
                HtmlConverter.map(
                        STYLE_ATTRIBUTE_HEADER_LEVEL, 
                        lvl -> HtmlTag.tag("<h" + lvl + " class=\""+CSS_CLASS+"\">", "</h"+lvl+">")
                )
        );
    }

    /**
     * Get HTML version of document.
     * @param options the conversiono options to use.
     * @return the HTML for this document
     */
    public String getHtml(HtmlConversionOption... options) {
        return getHtml(Arrays.asList(options));
    }

    /**
     * Get standalone HTML version of document, i. e. complete with header and CSS definitions.
     * @param options the conversiono options to use.
     * @return the HTML for this document
     */
    public String getStandaloneHtml(HtmlConversionOption... options) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                  "<head>\n" +
                  "<title>"+TextUtil.escapeHTML(root.getName())+"</title>\n" +
                  "<style type=\"text/css\">\n");
        sb.append("body { ").append(getRoot().getFontStyle().getFontDef().getCssStyle()).append(" }\n");
        sb.append(getCss());
        sb.append("</style>\n");
        sb.append("</head>\n");
        sb.append(getHtml(Arrays.asList(options)));
        sb.append("</body>\n</html>\n");
        return sb.toString();
    }

    /**
     * Get section by name.
     * @param name the section name
     * @return Optinal holding the section if present
     */
    public Optional<Section> getSection(String name) {
        return getRoot().getSubSection(name);
    }

    @Override
    public String toString() {
        return "SDocument{" +
               "root=" + root +
               '}';
    }

}
