/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.Node;
import com.dua3.utility.lang.LangUtil;
import com.dua3.utility.text.RichText;
import com.dua3.utility.text.RichTextBuilder;
import com.dua3.utility.text.Style;

import java.util.*;
import java.util.logging.Logger;

/**
 * Class for a section in a document in the S-Model.
 * <p>
 * A section has a level and a name and contains text, possibly followed by sub sections. Consider a HTML document.
 * A section starts with a header tag which also indicates the section name.<br>
 * **Example:** (indentation only for demostration)
 * {@code
 * <h1>section with level 1</h1>
 *   This text belongs to the section at level 1.
 *   <h2>this is a sub section</h2>
 *     This text belongs to the subsection.
 * <h1>another section with level 1</h1>
 *   This section is distinct from the first.
 *   This text belongs to the second section with level 1.
 * }
 */
public class Section extends Node {
    
    private static final Logger LOG = Logger.getLogger(Section.class.getName());

    /** Section level. */
    private final int level;
    /** Section name. */
    private final String name;
    /** Section name. */
    private final String title;
    /** List of subsections. */
    private final Map<String, Section> subSections = new LinkedHashMap<>();
    /** The section's body font. */
    private final Style fontStyle;

    private boolean consistent = true;
    private int headerEnd = -1;
    
    /** The document this section belongs to. */
    private final Document document;

    /**
     * Create new section.
     * @param parent the parent node
     * @param level the section level
     * @param name the section name
     * @param title the section title
     * @param fontStyle the section body font style
     */
    public Section(Document document, Node parent, int level, String name, String title, Style fontStyle) {
        super(parent);
        this.document = Objects.requireNonNull(document);
        this.level = level;
        this.name = Objects.requireNonNull(name);
        this.title = Objects.requireNonNull(title);
        this.fontStyle = Objects.requireNonNull(fontStyle);
    }

    /**
     * Get section level.
     * @return the level of this section
     */
    public int getLevel() {
        return level;
    }

    /**
     * Get section name
     * @return the name of this section
     */
    public String getName() {
        return name;
    }

    /**
     * Add a subsection to this section.
     * @param section the subsection to add
     * @throws IllegalArgumentException if the subsection's level is less than or equal to this section's level
     */
    public void addSection(Section section) {
        LOG.finer(() -> "adding subsection '"+section.getName()+"' to section '"+name+"'");
        
        LangUtil.check(
                section.getLevel() > level, 
                () -> new IllegalArgumentException(String.format("level of subsection must be greater than this section's level (%d): %d", level, section.getLevel()))
        );

        if (section.getLevel()!=level+1) {
            LOG.fine(() -> "subsection to add should have level "+(level+1)+" but has "+section.getLevel());    
        }
        
        add(section);
        Section v = subSections.putIfAbsent(section.name, section);
        
        if (v!=null) {
            LOG.warning("section '"+name+"': duplicate subsection '"+section.getName()+"' will be ignored in section list");
            consistent = false;
        }
    }

    @Override
    public boolean isConsistent() {
        return consistent && super.isConsistent();
    }

    /**
     * Get this section's subsections
     * @return collection containing this section's subsections
     */
    public Collection<Section> getSubSections() {
        return subSections.values();
    }

    /**
     * Get subsection by name.
     * @param name the name of the subsection
     * @return Optional holding the subsection if present
     */
    public Optional<Section> getSubSection(String name) {
        return Optional.ofNullable(subSections.get(name));
    }
    
    @Override
    public String toString() {
        return "SSection{" +
               "level=" + level +
               ", title='" + title + '\'' +
               ", name='" + name + '\'' +
               ", subSections=" + subSections +
               ", consistent=" + consistent +
               '}';
    }

    @Override
    public void appendTo(RichTextBuilder rtb) {
        appendHeaderTo(rtb);

        for (Node child: contentNodes()) {
            child.appendTo(rtb);
        }
    }

    /**
     * Append text content of this section's text content (i. e. without subsections).
     * @param rtb the Appendable to append to
     */
    public void appendTextContentTo(RichTextBuilder rtb) {
        appendHeaderTo(rtb);

        for (Node child: contentNodes()) {
            if (!(child instanceof Section)) {
                child.appendTo(rtb);
            }
        }
    }

    private void appendHeaderTo(RichTextBuilder rtb) {
        if (level > 0) {
            Style style = document.getHeaderStyle(level);
            rtb.push(style);
            rtb.append(title);
            rtb.pop(style);
            rtb.append("\n");
        }
    }

    /**
     * Get text for this section.
     * @return text for this section
     */
    public RichText getText() {
        RichTextBuilder rtb = new RichTextBuilder();
        appendTo(rtb);
        return rtb.toRichText();
    }

    /**
     * Get text content of this section (i. e. without sub sections). 
     * @return this section's text content
     */
    public RichText getTextContent() {
        RichTextBuilder rtb = new RichTextBuilder();
        appendTextContentTo(rtb);
        return rtb.toRichText();
    }
    
    /**
     * Get childnodes that are part of the header.
     * @return the header notes
     */
    public List<Node> headerNodes() {
        List<Node> nodes = children();
        return nodes.subList(0, headerEnd);
    }

    /**
     * Get childnodes that are part of the content.
     * @return the content notes
     */
    public List<Node> contentNodes() {
        List<Node> nodes = children();
        return nodes.subList(headerEnd, nodes.size());
    }

    /**
     * Mark the end of the header.
     * <p>
     * When a new section is created, nodes that are added using {@link #add(Node)} are considered to be part of
     * the header until this method is called. All nodes that are added afterwards are considered content nodes.
     */
    public void markHeaderEnd() {
        LangUtil.check(headerEnd==-1, "header end already marked");
        headerEnd = children().size();
    }

    @Override
    public Style getFontStyle() {
        return fontStyle;
    }
    
}
