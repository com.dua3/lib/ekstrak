/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.Node;
import com.dua3.utility.data.Pair;
import com.dua3.utility.text.RichTextBuilder;
import com.dua3.utility.text.Style;

import java.util.Map;

/**
 * Line end marker node in the S-Model.
 * <p>
 * NOTE: instances of this class cannot have children.
 */
public class LineEnd extends Node {

    public static final String STYLE_NAME_LINE_END = "line_marker";
    public static final String ATTR_LINE_END = "line_end";
    public static final String ATTR_VALUE_LINE_END_LINE_EOL = "end_of_line";
    public static final Style STYLE_LINE_END = Style.create(STYLE_NAME_LINE_END, Map.entry(ATTR_LINE_END, ATTR_VALUE_LINE_END_LINE_EOL));

    /**
     * Constructor.
     * @param parent the parent node
     */
    public LineEnd(Node parent) {
        super(parent);
    }

    @Override
    public void appendTo(RichTextBuilder rtb) {
        rtb.push(STYLE_LINE_END);
        rtb.append("\n");
        rtb.pop(STYLE_LINE_END);
    }

    @Override
    public void add(Node child) {
        throw new UnsupportedOperationException(getClass().getSimpleName()+" is a marker class and cannot contain child nodes");
    }
}
