/*
 * Copyright 2020 Axel Howind
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.dua3.ekstrak;

import com.dua3.ekstrak.model.PLine;
import com.dua3.utility.data.Color;
import com.dua3.utility.io.IOUtil;
import com.dua3.utility.text.Font;
import com.dua3.utility.text.FontDef;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;
import java.util.function.ToIntFunction;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PdfParserTest {

    @Test
    public void testParse() throws IOException {
        Font defaultFont = new Font(
            ".SFNS",
            19f,
            Color.BLACK,
            false,
            false,
            false,
            false
        );
        
        ToIntFunction<PLine> getSectionLevel = line -> {
            Font font = PdfParser.getFont(line.style().getFontStyle());
            if (font!=null && !line.getText().isEmpty()) {
                if (font.isBold() && font.getSizeInPoints()==37f) {
                    return 1;
                }
                if (font.isBold() && font.getSizeInPoints()==24f) {
                    return 2;
                }
                if (font.toString().equals("EPROVX+.SFNS-Regular_opsz13E65F_GRAD1900000_YAXS1900000_wght2B@19.0")) {
                    return 3;
                }
            }
            return 0;
        };

        Function<String, String> sectionMapper = name -> name;
        
        PdfParser parser = PdfParser.create(
                PdfParser.useStandardFontNames(false),
                PdfParser.defaultFont(defaultFont),
                PdfParser.sectionLevelMapper(getSectionLevel),
                PdfParser.sectionMapper(sectionMapper)
        );
        
        Document doc;
        try(InputStream in = getClass().getResourceAsStream("Universal Declaration of Human Rights | United Nations.pdf")) {
            doc = parser.parse(in);
        }
        
        // test the conversion
        String actual = doc.getHtml();
        String expected = IOUtil.read(getClass().getResource("Universal Declaration of Human Rights | United Nations - textonly.html"), StandardCharsets.UTF_8);
        assertEquals(expected, actual);

        // also test that line ends are marked correctly and die HtmlConversionOption works
        String actual2 = doc.getHtml(PdfParser.keepLineBreaks(true));
        String expected2 = expected.replaceAll("\n", "<br>\n").replaceAll("(</h[0-9]>)<br>", "$1");
        assertEquals(expected2, actual2);
    }

    @Test
    public void testStandaloneHtml() throws IOException {
        Font defaultFont = new Font(
            ".SFNS",
            19f,
            Color.BLACK,
            false,
            false,
            false,
            false
        );
        
        Function<String, String> sectionMapper = name -> name;
        ToIntFunction<PLine> getSectionLevel = line -> {
            Font font = PdfParser.getFont(line.style().getFontStyle());
            if (font!=null && !line.getText().isEmpty()) {
                if (font.isBold() && font.getSizeInPoints()==37f) {
                    return 1;
                }
                if (font.isBold() && font.getSizeInPoints()==24f) {
                    return 2;
                }
                if (font.toString().equals("EPROVX+.SFNS-Regular_opsz13E65F_GRAD1900000_YAXS1900000_wght2B@19.0")) {
                    return 3;
                }
            }
            return 0;
        };
        
        PdfParser parser = PdfParser.create(
                PdfParser.useStandardFontNames(false),
                PdfParser.defaultFont(defaultFont),
                PdfParser.headerStyle(1, FontDef.parseFontspec(".SFNS-bold-37.0-#000000")),
                PdfParser.headerStyle(2, FontDef.parseFontspec(".SFNS-bold-24.0-#000000")),
                PdfParser.headerStyle(3, FontDef.parseFontspec(".SFNS-19.0-#000000")),
                PdfParser.sectionLevelMapper(getSectionLevel),
                PdfParser.sectionMapper(sectionMapper)
        );
        
        Document doc;
        try(InputStream in = getClass().getResourceAsStream("Universal Declaration of Human Rights | United Nations.pdf")) {
            doc = parser.parse(in);
        }
        
        // test the conversion
        String actual = doc.getStandaloneHtml();
        String expected = IOUtil.read(getClass().getResource("Universal Declaration of Human Rights | United Nations.html"), StandardCharsets.UTF_8);
        assertEquals(expected, actual);
    }
}
